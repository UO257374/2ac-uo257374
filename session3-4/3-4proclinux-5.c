// This source file must be compiled with the following command:
//   gcc -Wall 3-4proclinux-5.c 3-4printvm_mod.c -o 3-4proclinux-5 -lmem

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

extern void print_virtual_physical_pte();

int main(void)
{
  void *p;         /* Pointer to the requested memory area */

  ////////////////////////////////////
  // A 16-KiB memory area is requested
  ////////////////////////////////////

  if ((p = malloc(16*1024)) == NULL)
    {
      fprintf(stderr, "Error in malloc\n");
      return(-1);
    }

  printf("\nProcess Identificator (PID): %d\n", getpid());
    
  
  ////////////////////////////////////////////////////////////////
  // What is the physical address associated with the memory area?
  ////////////////////////////////////////////////////////////////
  print_virtual_physical_pte(p, "The requested memory area\n"
                                 "-----------------------------\n");

  printf("\n---- Press [ENTER] to continue");
  getchar();

  /////////////////////////////////////
  // The 16-KiB memory are is released
  /////////////////////////////////////
  free(p);

  return 0;
}
